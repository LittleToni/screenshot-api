# Screenshot API - Service Container

Simple self hosted API using sxpress and puppeteer for taking screenshots of the given site.

## Requirements

- docker
- docker-compose

## Installation

- run ```docker-compose build``` to build the images
- run ```docker-compose up``` to start container
- run ```docker-compose down``` to stop container

## Example

Open http://localhost:3000/?url=https://google.de to get a screenshot of google homepage.
Extend the url with the query paramater "type" to define an individual image format (supported png|jpg|jpeg).

## Usefull

- https://pptr.dev/
- https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md
