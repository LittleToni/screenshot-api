'use strict'

const express = require('express');
const app = express();
const puppeteer = require('puppeteer');
const validation = require('./validation');

const config = {
    port: 3000,
    host: '0.0.0.0'
}

app.get('/', async (req, res) => {
    let type = validation.type(req.query.type);
    let url = validation.url(req.query.url);

    if (url) {
        // Launch headless Chromium Browser
        // Todo: Check --no-sandbox flag as security issue
        // Since Chrome 65 launch the browser with the --disable-dev-shm-usage flag
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-dev-shm-usage'
            ]
        })

        const page = await browser.newPage();

        await page.goto(url);
        const image = await page.screenshot({
            fullPage: true,
            type: type
        });

        await browser.close();

        res.set('Content-Type', 'image/' + type);
        res.send(image);
    } else {
        res.status(422).send('Unprocessable Entity');
    }
})

app.listen(config.port, config.host, (e) => {
    if (e) {
        throw new Error('Internal Server Error');
    }
})

