'use strict';

const validUrl = require('valid-url');
const validTypes = ['png', 'jpg', 'jpeg'];

module.exports = {
    /**
     * Validate request param type. Return png as default type.
     *
     * @param type
     * @returns {string|*}
     */
    type(type) {
        if (validTypes.indexOf(type) > -1) {
            return type;
        }

        return 'png';
    },

    /**
     * Validate request param url. Return false if url is not valid.
     *
     * @param url
     * @returns {boolean|*}
     */
    url(url) {
        console.log(url);

        if (validUrl.isWebUri(url)) {
            return url;
        }

        return false;
    }
}
